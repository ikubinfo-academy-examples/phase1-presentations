package com.ikubinfo.presentations.collections;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class MapTest {

	public static void main(String p[]) {

		Student beni = new Student(1, "Beni");
		Student mira = new Student(2, "Mira");
		Student moza = new Student(3, "Moza");

		List<Student> studentArrayList = new ArrayList<>();
		studentArrayList.add(beni);
		studentArrayList.add(mira);
		studentArrayList.add(moza);
		/*
		 * studentArrayList.add(moza);//allows duplicate elements
		 */

		Map<Integer, String> studentMap = studentArrayList.stream()
				.collect(Collectors.toMap(Student::getStudentId, Student::getStudentName));

		System.out.println(studentMap);

		System.out.println("Getting id 1 name " + studentMap.get(1));

		System.out.println("Removing id 1");

		studentMap.remove(1);

		System.out.println(studentMap);

		if (studentMap.containsValue("Moza")) {

			System.out.println("Its you again Moza :P");

			
			
			
		}

		

	}
}
