package com.ikubinfo.presentations.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class IterableTest {

	public static void main(String ar[]) {

		Student beni = new Student(1, "Beni");
		Student mira = new Student(2, "Mira");
		Student moza = new Student(3, "Moza");

		List<Student> studentArrayList = new ArrayList<>();
		studentArrayList.add(beni);
		studentArrayList.add(mira);
		studentArrayList.add(moza);

		Iterable<Student> studentIterable = studentArrayList;

		/*
		 * Iterable : An interface that can be iterated over. That is, one that has a
		 * notion of
		 * "get me the first thing, now the next thing, and so on, until we run out."
		 * 
		 * Iterator : A class that manages iteration over an iterable. That is, it keeps
		 * track of where we are in the current iteration, and knows what the next
		 * element is and how to get it
		 */

		Iterator<Student> studentIterator = studentIterable.iterator();

		while (studentIterator.hasNext()) {
			Student student = studentIterator.next();
			if ("Mira".equals(student.getStudentName())) {

				studentIterator.remove();
			}
		}

		System.out.println("After deleting Mira" + studentArrayList);

		studentIterable.forEach((Student student) -> {
			System.out.println("Student Names using forEach " + student.getStudentName());
		});

		// Collections example
		Collection<Student> studentCollection = studentArrayList;
		System.out.println("Lets add Mira again");
		studentCollection.add(mira);

		studentCollection.remove(moza);

		System.out.println("Checking if Collection is empty");

		if (!studentCollection.isEmpty()) {
			System.out.println("Collection not empty,Checking if Student Moza exists");
			System.out.println("Moza exists " + studentCollection.contains(beni));
		}

	}

}
