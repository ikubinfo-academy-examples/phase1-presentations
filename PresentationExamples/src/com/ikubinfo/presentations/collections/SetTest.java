package com.ikubinfo.presentations.collections;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SetTest {
	public static void main(String ar[]) {

		Student beni = new Student(1, "Beni");
		Student mira = new Student(2, "Mira");
		Student moza = new Student(3, "Moza");

		List<Student> studentArrayList = new ArrayList<>();
		studentArrayList.add(beni);
		studentArrayList.add(mira);
		studentArrayList.add(moza);
		studentArrayList.add(moza);// allows duplicate elements

		System.out.println("Printing as List" + studentArrayList);

		System.out.println("Converting to Set");

		Set<Student> studentSet = new HashSet<Student>(studentArrayList);

		System.out.println("Printing as a Set" + studentSet);

		System.out.println("Trying to add moza again ");

		if (studentSet.add(moza)) {
			System.out.println("Moza youre in ");

		} else {
			System.out.println("Sorry Moza");
		}

	}
}
