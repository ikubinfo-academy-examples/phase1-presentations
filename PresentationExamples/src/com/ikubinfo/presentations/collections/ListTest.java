package com.ikubinfo.presentations.collections;

import java.util.ArrayList;
import java.util.List;

public class ListTest {

	public static void main(String[] args) {
		
		Student beni = new Student(1, "Beni");
		Student mira = new Student(2, "Mira");
		Student moza = new Student(3, "Moza");

		List<Student> studentArrayList = new ArrayList<>();
		studentArrayList.add(beni);
		studentArrayList.add(mira);
		studentArrayList.add(moza);
		studentArrayList.add(moza);//allows  duplicate elements
		
		System.out.println("Getting first Student");
		
		Student first=studentArrayList.get(0);
		
		System.out.println("First student is "+first.getStudentName());
		
		
		System.out.println("Lets add another student");
		
		studentArrayList.add(first);
		
		System.out.println("Using index "+studentArrayList.indexOf(first));
		
		
		System.out.println("Using lastIndexOf "+studentArrayList.lastIndexOf(first));

	}

}
